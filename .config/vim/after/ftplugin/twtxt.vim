" Override global setting {{{

" allow twtxt.vim to identify twtxt files
set syntax=twtxt

" Override global setting }}}

" Configure twtxt.vim {{{

packadd twtxt

" Configure twtxt.vim }}}

