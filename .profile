#!/bin/sh

#
# ~/.profile
#

# Profile setup {{{

# Load environment variables
[ -f ~/.config/sh/xdg ] && . ~/.config/sh/xdg
[ -f ~/.config/sh/env ] && . ~/.config/sh/env

import() {
	if [ -f "$1" ]
	then
		. "$1"
	elif [ -d "${XDG_CONFIG_HOME}" ] && [ -f "${XDG_CONFIG_HOME}/$1" ]
	then
		. "${XDG_CONFIG_HOME}/$1"
	elif [ -d "${XDG_LIB_HOME}" ] && [ -f "${XDG_LIB_HOME}/$1" ]
	then
		. "${XDG_LIB_HOME}/$1"
	fi
}

using() {
	if [ -d "$1" ]
	then
		export PATH="$1:${PATH}"
	elif [ -d "${XDG_BIN_HOME}" ] && [ -d "${XDG_BIN_HOME}/$1" ]
	then
		export PATH="${XDG_BIN_HOME}/$1:${PATH}"
	fi
}

# Profile setup }}}

# Import external scripts {{{

# Include alias definitions
import "aliases/cd"
import "aliases/editor"
import "aliases/go"
import "aliases/ls"
import "aliases/mkdir"
import "aliases/twtxt"

# Import external scripts }}}

# Update PATH {{{

# Add dotnet tools to PATH
using "${DOTNET_TOOLS}"

# Add go lang tools to PATH
using "${GOROOT}/bin"
using "${GOBIN}"

# Add OS specific scripts to PATH
using "$(uname | tr '[:upper:]' '[:lower:]')"

# Update PATH }}}

# Run startup scripts {{{

# Disable byobu session
if [ -x "$(command -v byobu-disable)" ]
then
	byobu-disable || true
fi


# Startup ssh-agent(1)
if [ -z "$SSH_AUTH_SOCK" ]
then
	eval "$(ssh-agent -s)" >/dev/null 2>&1
fi

# Run startup scripts }}}

