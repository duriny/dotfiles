# dotfiles

My dotfiles, unstable, but not unusable. Please enjoy if you want.

## Installation

This repo is designed to be installed directly into a user's `$HOME` directory.
Once installed, all that is needed to update your local copy is `git pull`.

### Clone Repo

```sh
git init
git remote add origin git@git.envs.net:duriny/dotfiles.git
git fetch
git checkout origin/main -ft
```

## Submodules

To update the submodules, recursively update all the submodules and commit the
update.

### Initialise submodules

```sh
git submodule update --init --recursive
```

### Update submodules

```sh
git submodule update --recursive --remote
git commit -am "Updated submodules"
```

## Troubleshooting

### Ensure ssh config is correct

```sh
chmod 600 ~/.ssh/config
```

## License

See [LICENSE](LICENSE) for terms.
